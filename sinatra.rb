require 'sinatra'
require 'rack/throttle'
require_relative 'nanpa_api'

use Rack::Throttle::Second,   :max => 2

get '/' do
  redirect('https://bitbucket.org/megraf/nanpa_area_code_look_up')
end

get '/request' do
  params[:city]
  params[:state]
  NanpiAPI.request(params[:city],params[:state])

end

