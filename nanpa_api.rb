require 'rest-client'
require 'nokogiri'
require 'json'

# NANPI (North American Number Planning Association) unofficial API for Area Code Lookups.
# https://www.nationalnanpa.com/

class NanpiAPI

  def self.request(city, state)

    response_hash = Hash.new {|h, k| h[k]=[]}
    responses = []

    response = fetch(city, state)
    matches = response.css('tr')[1..-1]

    # This block and below could be more efficient,
    # no need to double loop
    matches.css('tr > td').children.each do |child|
      responses << child.text
    end
    # slice our array into 4 smaller arrays, excludes the last item;
    # sets and appends array elements to response_hash
    responses[0...-1].each_slice(4).to_a.each do |response|
      response_hash[:area_codes] |= [response[0].match(/([A-Z]|[0-9])\w+/)[0].to_i]
      response_hash[:cities]     |= [response[1].gsub(' ','')]
      response_hash[:counties]   |= [response[2].gsub(' ','')]
      response_hash[:states]     |= [response[3].gsub(' ','')]
    end

    response_hash.empty? ? {:error => 'no results found'}.to_json : response_hash.to_json

  end

  def self.fetch(city, state)

    # randomizes session-id values to prevent collisions if this
    # actually picks up traction
    ran_gen = rand(9999999999).to_s.rjust(10,'0')

    host = 'www.nationalnanpa.com'
    user_agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.12; rv:53.0) Gecko/20100101 Firefox/53.0'
    accept = 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'
    accept_lang = 'en-US,en;q=0.5'
    accept_encod = 'gzip, deflate, br'
    referer = 'https://www.nationalnanpa.com/enas/npa_city_query.do'
    cookie = "JSESSIONID=TSISt7LVUlwnLbEYm19IQVrWiZDLHq33mw3W2swuR9KP1RciD8Zr!-#{ran_gen}; AWSELB=1DF155A30C6E35C74987FBA23238CE2CD48D8D447EF60604E0F95B9986DF482DEB2A0615C4B408CBA844DC28501549B2CAD2CD0089C30D06C6991C8D7C825E72FF2C8FB221"
    # Parses the post response with Nokogiri HTML module
    response = Nokogiri::HTML(
        RestClient.post(
            'https://www.nationalnanpa.com/enas/displayNpaCityReport.do',
            {city: city.strip.upcase, stateAbbr: state.strip.upcase, npaId: ''},
            {'Host' => host, 'User-Agent' => user_agent, 'Accept' => accept,
             'Accept-Language' => accept_lang, 'Accept-Encoding' => accept_encod,
             'Referer' => referer, 'Cookie' => cookie
            }
        )
    )

    response.xpath('//*[@id="searchNpaToCityForm"]').css('table')
  end

end