# NANPA unofficial area code lookup API

# Features
  - Area code look up with City / State
  - Uses NANPA database
  - returns JSON
  - Rate limiting
  - Session Management

# Access

There are Access ways to use the project:

* [Clone] this repository
* [Visit] the project on Heroku

### Installation

If you've cloned this repository - 

```sh
$ cd *repository_location*/
$ bundle install
$ rake config.ru
$ localhost:9292/request
```

   [clone]: <https://megraf@bitbucket.org/megraf/nanpa_area_code_look_up.git>
   [visit]: <https://protected-dawn-49952.herokuapp.com/>
   
## Request Structure

https://protected-dawn-49952.herokuapp.com/request?city=(CITY+HERE)&state=(TWO+DIGIT+STATE)

Example:

```
https://protected-dawn-49952.herokuapp.com/request?city=austin&state=tx
```

```
https://protected-dawn-49952.herokuapp.com/request?city=naples&state=fl
```